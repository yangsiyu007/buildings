import os
from skimage import io, transform
import numpy as np
from torch.utils.data import Dataset, DataLoader
from torchvision import transforms, utils

# Ignore warnings
import warnings
warnings.filterwarnings("ignore")

class SpaceNetDataset(Dataset):
    """SpaceNet dataset."""

    def __init__(self, root_dir, splits, transform=None):
        """
        Args:
            root_dir (string): Directory containing folder annotations and .txt files with the train/val/test splits
            splits: ['trainval', 'test']
            transform (callable, optional): Optional transform to be applied
                on a sample.
        """
        self.root_dir = root_dir
        self.transform = transform
        self.image_list = []
        self.xml_list = []

        data_files = []
        for split in splits:
            with open(os.path.join(root_dir, split + '.txt')) as f:
                data_files.extend(f.read().splitlines())

        for line in data_files:
            line = line.split(' ')

            image_name = line[0].split('/')[-1]
            xml_name = line[1].split('/')[-1]

            self.image_list.append(image_name)
            self.xml_list.append(xml_name)

    def __len__(self):
        return len(self.image_list)

    def __getitem__(self, idx):
        img_path = os.path.join(self.root_dir, 'annotations', self.image_list[idx])
        target_path = os.path.join(self.root_dir, 'annotations', img_path.replace('.jpg', 'segcls.png'))

        image = io.imread(img_path)
        target = io.imread(target_path)
        target[target == 100] = 1  # building interior
        target[target == 255] = 2  # border

        sample = {'image': image, 'target': target, 'image_name': self.image_list[idx]}

        if self.transform:
            sample = self.transform(sample)

        return sample
