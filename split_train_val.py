root_path = '/home/lxhfirenking/work/cs231n/project/data/Vegas_8bit_256/'
trainval_path = root_path + 'trainval.txt'
train_path = root_path + 'train.txt'
val_path = root_path + 'val.txt'

with open(trainval_path, 'r') as f:
    trainval = f.read().splitlines()

len_val = int(0.125 * len(trainval))
len_train = len(trainval) - len_val

train_set = trainval[:len_train]
val_set = trainval[len_train:]

with open(train_path, 'w') as f:
    for line in train_set:
        f.write('{}\n'.format(line))

with open(val_path, 'w') as f:
    for line in val_set:
        f.write('{}\n'.format(line))
