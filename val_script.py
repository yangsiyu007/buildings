import torch
import torch.nn.functional as F
import torchvision.datasets as dset
import torchvision.transforms as T
import os
import matplotlib.pyplot as plt
import numpy as np


from torch.utils.data import DataLoader
from torch.utils.data import sampler
from utils import segment_and_visualize
from models.unetv2.unetv2 import Unet
from models.unetv2.unetv2_base import UnetBase
from dataset import SpaceNetDataset
from data_transforms import ToTensor
from skimage import io

# metadata setup
USE_GPU = True

# first try
#cp_path_list = ['/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch0_2018-05-26-20-11-16.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch1_2018-05-26-20-22-34.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch2_2018-05-26-20-33-50.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch3_2018-05-26-20-45-06.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch4_2018-05-26-20-56-22.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch5_2018-05-26-21-07-38.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch6_2018-05-26-21-18-54.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch7_2018-05-26-21-30-11.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch8_2018-05-26-21-41-28.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch9_2018-05-26-21-52-44.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch10_2018-05-27-00-36-29.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch11_2018-05-27-00-47-44.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch12_2018-05-27-00-58-59.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch13_2018-05-27-01-10-14.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch14_2018-05-27-01-21-29.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch15_2018-05-27-01-32-44.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch16_2018-05-27-01-44-00.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch17_2018-05-27-01-55-15.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch18_2018-05-27-02-06-30.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch19_2018-05-27-02-17-46.pth.tar',]

#base line
#cp_path_list = ['/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch0_2018-05-29-05-39-49.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch1_2018-05-29-05-49-55.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch2_2018-05-29-05-54-36.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch3_2018-05-29-05-59-17.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch4_2018-05-29-06-03-59.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch5_2018-05-29-06-08-39.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch6_2018-05-29-06-13-20.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch7_2018-05-29-06-18-01.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch8_2018-05-29-06-22-42.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch9_2018-05-29-06-27-23.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch10_2018-05-31-03-48-14.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch11_2018-05-31-03-52-52.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch12_2018-05-31-03-57-31.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch13_2018-05-31-04-02-10.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch14_2018-05-31-04-06-49.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch15_2018-05-31-04-11-28.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch16_2018-05-31-04-16-07.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch17_2018-05-31-04-20-46.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch18_2018-05-31-04-25-25.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch19_2018-05-31-04-30-04.pth.tar',]

# unetv2 equal weights
#cp_path_list = ['/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch0_2018-05-31-06-12-58.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch1_2018-05-31-06-24-18.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch2_2018-05-31-06-35-38.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch3_2018-05-31-06-46-57.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch4_2018-05-31-06-58-17.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch5_2018-05-31-07-09-36.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch6_2018-05-31-07-20-56.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch7_2018-05-31-07-32-16.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch8_2018-05-31-07-43-35.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch9_2018-05-31-07-54-54.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch10_2018-06-01-04-10-05.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch11_2018-06-01-04-21-29.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch12_2018-06-01-04-32-48.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch13_2018-06-01-04-44-07.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch14_2018-06-01-04-55-26.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch15_2018-06-01-05-06-45.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch16_2018-06-01-05-18-05.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch17_2018-06-01-05-29-24.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch18_2018-06-01-05-40-43.pth.tar',
#                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch19_2018-06-01-05-52-02.pth.tar',]

# baseline interior weights
cp_path_list = [
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch0_2018-06-02-19-37-46.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch1_2018-06-02-19-42-27.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch2_2018-06-02-19-47-07.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch3_2018-06-02-19-51-47.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch4_2018-06-02-19-56-27.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch5_2018-06-02-20-01-07.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch6_2018-06-02-20-05-47.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch7_2018-06-02-20-10-27.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch8_2018-06-02-20-15-06.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch9_2018-06-02-20-19-46.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch10_2018-06-02-20-24-26.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch11_2018-06-02-20-29-06.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch12_2018-06-02-20-33-45.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch13_2018-06-02-20-38-24.pth.tar',
                '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch14_2018-06-02-20-43-03.pth.tar',]


model_choice = 'unetbase' #'unetbase'
feature_scale = 1
dtype = torch.float32
split_tags = ['trainval', 'test'] #compatibility tag, should always stay like this
tag = 'train' # train val test
data_root_path = '/home/lxhfirenking/work/cs231n/project/data/Vegas_8bit_256_{0}/'.format(tag)

score_weights_tensor = torch.from_numpy(np.array([0.1, 0.8, 0.1])) # need to change this value is the corresponding value in train_script changes
batch_size = 20
num_workers = 10 # num_workers: how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process

dset = SpaceNetDataset(data_root_path, split_tags, transform=T.Compose([ToTensor()]))
loader = DataLoader(dset, batch_size=batch_size, shuffle=True, num_workers=num_workers) # shuffle True to reshuffle at every epoch
print ('{0} set size:{1}'.format(tag, len(dset)))

if USE_GPU and torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
print('using device:', device)


def check_accuracy(loader, model, print_every=10):
    print('Calculating {0} set performance...'.format(tag))

    model = model.to(device=device, dtype=dtype)
    model.eval()  # set model to evaluation mode
    score_weights = score_weights_tensor.to(device=device, dtype=dtype)
    
    total_loss = 0
    t = 0
    with torch.no_grad():
        for t, data in enumerate(loader):           
            x = data['image'].to(device=device, dtype=dtype)  # move to device, e.g. GPU
            y = data['target'].to(device=device, dtype=torch.long)
            scores = model(x)
            loss = F.cross_entropy(scores, y, score_weights)
            total_loss += loss.item()
            if t % print_every == 0:
                print('Iteration %d, loss running average = %.4f' % (t, total_loss / (t + 1)))
    return total_loss / (t + 1.0)

def main():
    print_every = 200

    loss_dict = {}

    for cp_path in cp_path_list:
        print("loading from checkoutpoint {0}".format(cp_path))
        checkpoint = torch.load(cp_path)

        if model_choice == 'unetv2':
            model = Unet(feature_scale=feature_scale, n_classes=3, is_deconv=True, in_channels=3, is_batchnorm=True)
        elif model_choice == 'unetbase':
            model = UnetBase(feature_scale=feature_scale, n_classes=3, is_deconv=True, in_channels=3, is_batchnorm=True)
        else:
            raise ValueError('Unknown model_choice={0}'.format(model_choice))

        model.load_state_dict(checkpoint['state_dict'])

        loss = check_accuracy(loader, model, print_every=print_every)
        print("Average loss on {0} set={1}".format(tag, loss))
        loss_dict[cp_path.split('/')[-1]] = loss

    for key, value in loss_dict.items():
        print("model:{0}       {1}_loss={2}".format(key, tag, value))

if __name__ == '__main__':
    main()