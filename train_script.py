import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torch.utils.data import DataLoader
from torch.utils.data import sampler

import torchvision.datasets as dset
import torchvision.transforms as T

import numpy as np

from models.unetv2.unetv2 import Unet
from models.unetv2.unetv2_base import UnetBase
from dataset import SpaceNetDataset
from utils import metrics, logger
from data_transforms import ToTensor
from time import localtime, strftime
import os

# metadata setup
USE_GPU = True

dtype = torch.float32
data_root_path_train = '/home/lxhfirenking/work/cs231n/project/data/Vegas_8bit_256_train/'
data_root_path_val = '/home/lxhfirenking/work/cs231n/project/data/Vegas_8bit_256_val/'
data_root_path_test = '/home/lxhfirenking/work/cs231n/project/data/Vegas_8bit_256_test/'
split_tags = ['trainval', 'test'] #compatibility tag, should always stay like this
train_batch_size = 10
val_batch_size = 4
test_batch_size = 4
num_workers = 4 # num_workers: how many subprocesses to use for data loading. 0 means that the data will be loaded in the main process
torch.backends.cudnn.benchmark=True

#weights adjustment
#score_weights_tensor = torch.from_numpy(np.array([0.1, 0.4, 0.5])) actual ratio of background:inner:border=0.769:0.158:0.073
#score_weights_tensor = torch.from_numpy(np.array([0.3, 0.3, 0.3]))
#background_w = 1.0 / 0.768
#interior_w = 1.0 / 0.158
#border_w = 1.0 / 0.073
#sum_w = background_w + interior_w + border_w
#score_weights_tensor = torch.from_numpy(np.array([background_w / sum_w, interior_w / sum_w, border_w / sum_w])) # baselinev16_statistic_weights

score_weights_tensor = torch.from_numpy(np.array([0.1, 0.8, 0.1])) # interior_weights


#experiment_name = 'baselinev16'
#experiment_name = 'unetv2_equal_weights'
#experiment_name = 'baselinev16_statistic_weights'
#experiment_name = 'baselinev16_interior_weights'
#experiment_name = 'unet_small_equal_weights'
#experiment_name = 'unet_small_interior_weights'
experiment_name = 'unetv2_interior_weights'
total_epochs = 20
model_choice = 'unetv2' #'unetbase' 'unetv2'
feature_scale = 1

#check point, uncomment to train from new
#starting_checkpoint_path = './checkpoints/unet_checkpoint_epoch9_2018-05-26-21-52-44.pth.tar'
#starting_checkpoint_path = './checkpoints/baselinev16/unet_checkpoint_epoch9_2018-05-29-06-27-23.pth.tar'
#starting_checkpoint_path = './checkpoints/unetv2_equal_weights/unet_checkpoint_epoch9_2018-05-31-07-54-54.pth.tar'
starting_checkpoint_path= ''

if USE_GPU and torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
print('using device:', device)

dset_train = SpaceNetDataset(data_root_path_train, split_tags, transform=T.Compose([ToTensor()]))

loader_train = DataLoader(dset_train, batch_size=train_batch_size, shuffle=True, num_workers=num_workers) # shuffle True to reshuffle at every epoch

dset_val = SpaceNetDataset(data_root_path_val, split_tags, transform=T.Compose([ToTensor()]))

loader_val = DataLoader(dset_val, batch_size=val_batch_size, shuffle=True, num_workers=num_workers)

dset_test = SpaceNetDataset(data_root_path_test, split_tags, transform=T.Compose([ToTensor()]))

loader_test = DataLoader(dset_test, batch_size=test_batch_size, shuffle=True, num_workers=num_workers)

print ('training set size:{0} validation set size:{1} test set size:{2}'.format(len(dset_train), len(dset_val), len(dset_test)))

def weights_init(m):
    if isinstance(m, nn.Conv2d):
        nn.init.kaiming_uniform(m.weight)
        m.bias.data.zero_()

def train_model(model, optimizer, epochs=1, print_every=10, checkpoint_path=""):
    """
    Train a model using the PyTorch Module API.
    
    Inputs:
    - model: A PyTorch Module giving the model to train.
    - optimizer: An Optimizer object we will use to train the model
    - epochs: (Optional) A Python integer giving the number of epochs to train for
    
    Returns: Nothing, but prints model accuracies during training.
    """
    model = model.to(device=device, dtype=dtype)  # move the model parameters to CPU/GPU
#     model.apply(weights_init)
    
    score_weights = score_weights_tensor.to(device=device, dtype=dtype)

    starting_epoch = 0
    if os.path.isfile(checkpoint_path):
        print("loading checkpoint from {0}".format(checkpoint_path))
        checkpoint = torch.load(checkpoint_path)
        model.load_state_dict(checkpoint['state_dict'])
        optimizer.load_state_dict(checkpoint['optimizer'])
        starting_epoch = checkpoint['epoch'] + 1
    else:
        print("No valid checkpoint is provided. Start to train from scratch...")

    # create checkpoint dir
    checkpoint_dir = './checkpoints/{0}'.format(experiment_name)
    if not os.path.exists(checkpoint_dir):
        os.makedirs(checkpoint_dir)

    for e in range(starting_epoch, epochs):
        print('Epoch {}'.format(e))
        for t, data in enumerate(loader_train):
            model.train()  # put model to training mode
            
            x = data['image'].to(device=device, dtype=dtype)  # move to device, e.g. GPU
            y = data['target'].to(device=device, dtype=torch.long)  # y is not a int value here; also an image

            scores = model(x)
            # TODO need to find backgroud/building pixel ratio for re-weighing
            loss = F.cross_entropy(scores, y, score_weights)
            #loss = F.cross_entropy(scores, y)

            # Zero out all of the gradients for the variables which the optimizer
            # will update.
            optimizer.zero_grad()

            # This is the backwards pass: compute the gradient of the loss with
            # respect to each  parameter of the model.
            loss.backward()

            # Actually update the parameters of the model using the gradients
            # computed by the backwards pass.
            optimizer.step()

            if t % print_every == 0:
                print('Iteration %d, training loss = %.4f' % (t, loss.item()))
                #check_accuracy(loader_val, model)
                print(flush=True)

        # save checkpoint
        checkpoint_path = '{0}/unet_checkpoint_epoch{1}_{2}.pth.tar'.format(checkpoint_dir, e, strftime("%Y-%m-%d-%H-%M-%S", localtime()))
        print('saving to checkout point file at {0}'.format(checkpoint_path))
        save_checkpoint({
            'epoch': e,
            'arch': 'UNet',
            'state_dict': model.state_dict(),
            'optimizer': optimizer.state_dict()
        }, checkpoint_path)

# TODO we need another class to transform this into the polygon thing 
def check_accuracy(loader, model):
    print('Calculating val set performance...')  

    model.eval()  # set model to evaluation mode
    score_weights = score_weights_tensor.to(device=device, dtype=dtype)
    
    with torch.no_grad():
        for t, data in enumerate(loader):           
            x = data['image'].to(device=device, dtype=dtype)  # move to device, e.g. GPU
            y = data['target'].to(device=device, dtype=torch.long)
            scores = model(x)

            loss = F.cross_entropy(scores, y, score_weights)
            print('Got val loss =', loss)
            break
           
            #_, preds = scores.max(1)
            #num_correct += (preds == y).sum()
            #num_samples += preds.size(0)
        #acc = float(num_correct) / num_samples
        #print('Got %d / %d correct (%.2f)' % (num_correct, num_samples, 100 * acc))

def save_checkpoint(state, path='./checkpoints/checkpoints.pth.tar'):
    torch.save(state, path)

def main():
    learning_rate = 0.5e-3
    num_classes = 3
    print_every = 50

    # improved model
    if model_choice == 'unetv2':
        model = Unet(feature_scale=feature_scale, n_classes=3, is_deconv=True, in_channels=3, is_batchnorm=True)
    # XD_XD baseline model
    elif model_choice == 'unetbase':
        model = UnetBase(feature_scale=feature_scale, n_classes=3, is_deconv=True, in_channels=3, is_batchnorm=True)

    # you can use Nesterov momentum in optim.SGD
    #optimizer = optim.SGD(model.parameters(), lr=learning_rate,
    #                     momentum=0.9, nesterov=True)
    optimizer = optim.Adam(model.parameters(), lr=learning_rate)

    train_model(model, optimizer, epochs=total_epochs, print_every=print_every, checkpoint_path=starting_checkpoint_path)

if __name__ == '__main__':
    main()