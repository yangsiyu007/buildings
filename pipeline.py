import numpy as np
import pandas as pd
import rasterio.features
import shapely.wkt
import shapely.ops
import shapely.geometry

from skimage import io

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.patches import Polygon
from matplotlib.collections import PatchCollection

import torch
from models.unetv2.unetv2 import Unet
from models.unetv2.unetv2_base import UnetBase
import os
from tqdm import tqdm

# Parameters
MIN_POLYGON_AREA = 150
BUFFER_SIZE = 3.0
USE_BUFFER = False

SAVE_PRED_POLYS = False

USED_MASK_TO_POLY_2 = False

dtype = torch.float32

USE_GPU = True
if USE_GPU and torch.cuda.is_available():
    device = torch.device('cuda')
else:
    device = torch.device('cpu')
print('using device:', device)

model_choice = 'unetbase' #'unetbase' #'unetv2'
feature_scale = 1 # 2 is the same as unetbase

image_folder = 'Vegas_8bit_256_val' # 'Vegas_exp'
other_folder = 'Vegas_8bit_256_val_poly' # 'Vegas_exp'

#cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_checkpoint_epoch19_2018-05-27-02-17-46.pth.tar'
#cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch9_2018-05-29-06-27-23.pth.tar'
#cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16/unet_checkpoint_epoch19_2018-05-31-04-30-04.pth.tar'
#cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_equal_weights/unet_checkpoint_epoch19_2018-06-01-05-52-02.pth.tar'
#cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_statistic_weights/unet_checkpoint_epoch14_2018-06-02-05-54-11.pth.tar'
#cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/baselinev16_interior_weights/unet_checkpoint_epoch14_2018-06-02-20-43-03.pth.tar'
#cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_small_equal_weights/unet_checkpoint_epoch14_2018-06-02-23-42-19.pth.tar'
#cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unet_small_interior_weights/unet_checkpoint_epoch14_2018-06-02-22-52-35.pth.tar'
cp_path = '/home/lxhfirenking/work/cs231n/project/bunny/buildings/checkpoints/unetv2_interior_weights/unet_checkpoint_epoch19_2018-06-03-04-00-35.pth.tar'

input_image_dir = '/home/lxhfirenking/work/cs231n/project/data/{}/annotations/'.format(image_folder) #'/home/lxhfirenking/work/cs231n/project/data/Vegas_8bit_256_val'

#predictions_dir = '/home/lxhfirenking/work/cs231n/project/data/{}/predictions'.format(other_folder)
predictions_dir = '/home/lxhfirenking/work/cs231n/project/data/{}/predictions_base'.format(other_folder)

#out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal_cp19.csv'.format(other_folder)
#out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal_cp9_base.csv'.format(other_folder)
#out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal2_cp19_base.csv'.format(other_folder)
#out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal_cp19_unetv2_equal_weights.csv'.format(other_folder)
#out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal2_cp14_base_statistic_weights.csv'.format(other_folder)
#out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal_cp14_base_interior_weights.csv'.format(other_folder)
#out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal_cp14_unet_small_equal_weights.csv'.format(other_folder)
#out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal_cp14_unet_small_interior_weights.csv'.format(other_folder)
out_path = '/home/lxhfirenking/work/cs231n/project/data/{}/proposal_cp14_unetv2_interior_weights.csv'.format(other_folder)

def visualize_poly(poly_list, mask, out_path):
    fig, ax = plt.subplots()
    ax.imshow(mask, alpha=0)  # don't show the mask, but need this to be added to the axes for polygons to show up
    patch_list = []

    for poly in poly_list:
        x, y = poly.exterior.coords.xy
        xy = np.column_stack((x, y))
        polygon = matplotlib.patches.Polygon(xy, linewidth=1, edgecolor='b', facecolor='none')
        patch_list.append(polygon)

    p = PatchCollection(patch_list, cmap=matplotlib.cm.jet, alpha=1)
    ax.add_collection(p)

    fig.savefig(out_path, bbox_inches='tight')
    plt.close(fig)

def to_shapely_shapes(shapes):
    shapely_shapes = []
    for shape, val in shapes:
        s = shapely.geometry.shape(shape).exterior

        if USE_BUFFER:
            s = shapely.geometry.polygon.Polygon(s.buffer(BUFFER_SIZE))
        else:
            s = shapely.geometry.polygon.Polygon(s)

        if s.area > MIN_POLYGON_AREA:
            shapely_shapes.append(s)

    return shapely_shapes


def get_contained_polygons(parent_poly, children_poly):
    contained_polygons = []
    for child_poly in children_poly:
        if parent_poly.contains(child_poly):
            contained_polygons.append(child_poly)
    return contained_polygons


def mask_to_poly2(mask, image_id, count_border_as_background=True):
    """
    Create polygons by selecting from polygons formed by counting border as background and
    counting border as building pixels.
    1. count border as background
    2. count border as building interior
    """
    mask1 = np.copy(mask)
    mask1[mask == 2] = 0  # as background

    mask2 = mask
    mask2[mask == 2] = 1  # as building

    shapes1 = rasterio.features.shapes(mask1.astype(np.int16), mask > 0)
    shapes2 = rasterio.features.shapes(mask2.astype(np.int16), mask > 0)

    shapes1 = to_shapely_shapes(shapes1)
    shapes2 = to_shapely_shapes(shapes2)

    polygons = []
    for polygon in shapes2:
        contained_polygons = get_contained_polygons(polygon, shapes1)
        if len(contained_polygons) > 1:
            polygons.extend(contained_polygons)
        else:
            polygons.append(polygon)

    mp = shapely.geometry.MultiPolygon(polygons)

    if isinstance(mp, shapely.geometry.Polygon):
        df = pd.DataFrame({
            'area_size': [mp.area],
            'poly': [mp],
            'image_id': [image_id]
        })
    else:
        df = pd.DataFrame({
            'area_size': [p.area for p in mp],
            'poly': [p for p in mp],
            'image_id': [image_id] * len(mp)
        })

    df = df.sort_values(by='area_size', ascending=False)
    df.loc[:, 'wkt'] = df.poly.apply(lambda x: shapely.wkt.dumps(x, rounding_precision=0))
    df.loc[:, 'bid'] = list(range(1, len(df) + 1))
    df.loc[:, 'area_ratio'] = df.area_size / df.area_size.max()
    return df, polygons


def mask_to_poly(mask, image_id, count_border_as_background=True):
    """
    Convert from 256x256 mask to polygons on the 256x256 image
    Adapted from original code: https://github.com/SpaceNetChallenge/BuildingDetectors_Round2/tree/master/1-XD_XD

    mask is a numpy array of shape (256, 256, 4) from io.imread(test_path)
    """
    # only need to sum across color channels if mask is read from a saved image
    # mask = np.sum(mask, axis=2) # make grey scale

    # for 'jet' colormap
    # if count_border_as_background:
    #     mask[mask == 775] = 408  # as background
    # else:
    #     mask[mask == 775] = 571  # as building

    if count_border_as_background:  # 0 background, 1 building, 2 border
        mask[mask == 2] = 0  # as background
    else:
        mask[mask == 2] = 1  # as building

    shapes = rasterio.features.shapes(mask.astype(np.int16), mask > 0)

    polygons = []
    for shape, val in shapes:
        s = shapely.geometry.shape(shape).exterior

        if USE_BUFFER:
            s = shapely.geometry.polygon.Polygon(s.buffer(BUFFER_SIZE))
        else:
            s = shapely.geometry.polygon.Polygon(s)

        if s.area > MIN_POLYGON_AREA:
            polygons.append(s)

    mp = shapely.geometry.MultiPolygon(polygons)

    if isinstance(mp, shapely.geometry.Polygon):
        df = pd.DataFrame({
            'area_size': [mp.area],
            'poly': [mp],
            'image_id': [image_id]
        })
    else:
        df = pd.DataFrame({
            'area_size': [p.area for p in mp],
            'poly': [p for p in mp],
            'image_id': [image_id] * len(mp)
        })

    df = df.sort_values(by='area_size', ascending=False)
    df.loc[:, 'wkt'] = df.poly.apply(lambda x: shapely.wkt.dumps(x, rounding_precision=0))
    df.loc[:, 'bid'] = list(range(1, len(df) + 1))
    df.loc[:, 'area_ratio'] = df.area_size / df.area_size.max()
    return df, polygons


def main(cp_path, input_image_dir, out_path, predictions_dir=None, save_pred=SAVE_PRED_POLYS):
    if predictions_dir:
        os.makedirs(predictions_dir, exist_ok=True)

    checkpoint = torch.load(cp_path)

    if model_choice == 'unetv2':
        model = Unet(feature_scale=feature_scale, n_classes=3, is_deconv=True, in_channels=3, is_batchnorm=True)
    elif model_choice == 'unetbase':
        model = UnetBase(feature_scale=feature_scale, n_classes=3, is_deconv=True, in_channels=3, is_batchnorm=True)
    else:
        raise ValueError('Unknown model_choice={0}'.format(model_choice))

    model.load_state_dict(checkpoint['state_dict'])
    model = model.to(device=device, dtype=dtype)
    model.eval()  # set model to evaluation mode
    print('Model checkpoint loaded')

    result_dfs = []

    image_files = os.listdir(input_image_dir)
    image_files = [image_file for image_file in image_files if image_file.endswith('.jpg')]

    for image_name in tqdm(image_files):
        image_name_no_file_type = image_name.split('.jpg')[0]

        image_id = image_name_no_file_type.split('RGB-PanSharpen_')[1]  # of the format _-115.3064538_36.1756826998
        image_path = os.path.join(input_image_dir, image_name)
        original_image = io.imread(image_path)

        image = original_image.transpose((2, 0, 1))
        image = torch.from_numpy(np.expand_dims(image, 0)).type(torch.float32).to(device=device, dtype=dtype)

        with torch.no_grad():
            scores = model(image)
            _, prediction = scores.max(1)

        prediction = prediction.reshape((256, 256)).cpu().data.numpy()

        if USED_MASK_TO_POLY_2:
            result_df, polygons = mask_to_poly2(prediction, image_id)
        else:
            result_df, polygons = mask_to_poly(prediction, image_id)
        result_dfs.append(result_df)

        # save prediction polygons visualization to output
        if save_pred and predictions_dir:
            visualize_poly(polygons, prediction, os.path.join(predictions_dir, 'poly_buff_' + image_name))

    all_df = pd.concat(result_dfs)

    print('Writing result to csv, length of all_df is {}'.format(len(all_df)))
    with open(out_path, 'w') as f:
        f.write('ImageId,BuildingId,PolygonWKT_Pix,Confidence\n')

        for i, row in tqdm(all_df.iterrows()):
            f.write("{},{},\"{}\",{:.6f}\n".format(
                row.image_id,
                int(row.bid),
                row.wkt,
                row.area_ratio))

if __name__ == '__main__':
    main(cp_path, input_image_dir, out_path, predictions_dir=predictions_dir)
